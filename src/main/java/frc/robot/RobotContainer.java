// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.pathplanner.lib.auto.AutoBuilder;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.Ampapult.AmpRollerDoNothing;
import frc.robot.commands.Ampapult.AmpRotationDoNothing;
import frc.robot.commands.Ampapult.AmpRotationRaw;
import frc.robot.commands.Ampapult.AmpSetAngle;
import frc.robot.commands.Ampapult.RollerSpin;
import frc.robot.commands.climber.Climb;
import frc.robot.commands.climber.ClimberDoNothing;
import frc.robot.commands.swerve.FieldDrive;
import frc.robot.commands.swerve.OffsetGyro;
import frc.robot.commands.three_in_one.ShootStaggered;
import frc.robot.commands.three_in_one.SpinThreeInOne;
import frc.robot.commands.three_in_one.ThreeInOneMouthDoNothing;
import frc.robot.commands.three_in_one.ThreeInOneRotDoNothing;
import frc.robot.commands.three_in_one.ThreeInOneRotateRaw;
import frc.robot.commands.three_in_one.ThreeInOneToAngle;
import frc.robot.subsystems.AmpapultRollers;
import frc.robot.subsystems.AmpapultRotation;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.ThreeInOneMouth;
import frc.robot.subsystems.ThreeInOneRot;
import swervelib.math.SwerveMath;

/**
 * this is the robot container. 
 * 
 * all subsytems are stored here, along with joysticks and auto functionality.
 */
public class RobotContainer {
  //inputs
  public static XboxController driverController = new XboxController(0);
  public static Joystick opJoystick = new Joystick(1);
  public static ButtonBoard buttonBoard = new ButtonBoard(2); 

  //3-in-1: shooter, intake, and amp
  public static ThreeInOneMouth mouth = ThreeInOneMouth.GetInstance();
  public static ThreeInOneRot rot3in1 = ThreeInOneRot.GetInstance();

  //drivetrain
  public static Drivetrain swerve = Drivetrain.GetInstance();
  
  //climbers
  public static Climber climber = Climber.GetInstance();

  //ampapult: backup amp mechanism
  public static AmpapultRollers rollers = AmpapultRollers.GetInstance();
  public static AmpapultRotation ampRot = AmpapultRotation.GetInstance();

  //auto selection dropdown
  private static SendableChooser<Command> autoSelect;

  public RobotContainer() {
    //field-centric drive command
    var drive = new FieldDrive(
      () -> SwerveMath.applyDeadband(
          -driverController.getLeftY(), 
          false, 0.01
      ), 
      () -> SwerveMath.applyDeadband(
        -driverController.getLeftX(), 
        false, 0.01
      ),
      () -> -driverController.getRightX(), 
      () -> true
    );

    //set default commands
    swerve.setDefaultCommand(drive);

    mouth.setDefaultCommand(new ThreeInOneMouthDoNothing());
    
    rollers.setDefaultCommand(new AmpRollerDoNothing());

    rot3in1.setDefaultCommand(new ThreeInOneRotDoNothing());

    ampRot.setDefaultCommand(new AmpRotationDoNothing());
    
    climber.setDefaultCommand(new ClimberDoNothing());


    //put auto dropdown on shufflboard
    autoSelect = AutoBuilder.buildAutoChooser();

    SmartDashboard.putData("Auto Selection", autoSelect);
    
    //setup controls
    configureBindings();
    driverBindings();

    //this makes autos more consistent.
    swerve.autoHelper.warmup();
  }

  /*
   * Xbox controller bindings:
   * 1 = A
   * 2 = B
   * 3 = X
   * 4 = Y
   * 5 = LB
   * 6 = RB
   * 7 = two square
   * 8 = menu
   * 9 = pressed left thumbstick
   * 10 = pressed right thumbstick
   */

  //mapped controls on the driver controller
  private void driverBindings() {
    new JoystickButton(driverController, 2)
      .onTrue(new OffsetGyro(() -> 0)); 
  }

  //mapped controls for operator controller (and button board)
  private void configureBindings() {
    
    // 3-in-1
    new JoystickButton(opJoystick,2) // SPIT
      .whileTrue(new ShootStaggered(ThreeInOneMouth.MOUTH_SPEED));

    new JoystickButton(opJoystick, 1) // SUCK
      .whileTrue(new ShootStaggered(-ThreeInOneMouth.MOUTH_SPEED));

    new JoystickButton(opJoystick, 12)
      .whileTrue(new ThreeInOneRotateRaw(0.3));

    new JoystickButton(opJoystick, 8)
      .whileTrue(new ThreeInOneRotateRaw(-0.3));

    new JoystickButton(opJoystick, 11) // HOME
      .whileTrue(new ThreeInOneToAngle(ThreeInOneRot.HOME_ANGLE, false)); 

    new JoystickButton(opJoystick, 9) // SHOOT ?
      .whileTrue(new ThreeInOneToAngle(ThreeInOneRot.SPEAKER_ANGLE, false));
      
    new JoystickButton(opJoystick, 7) // FLOOR ? 
      .whileTrue(new ThreeInOneToAngle(ThreeInOneRot.FLOOR_ANGLE, false));

    new JoystickButton(opJoystick, 10)
      .whileTrue(new ThreeInOneToAngle(ThreeInOneRot.AMP_ANGLE, false)); // also 13.25 works (18.45)  

    new JoystickButton(opJoystick, 4) 
      .whileTrue(new SpinThreeInOne(ThreeInOneMouth.MOUTH_SPEED/3.0));

    //climber 
    new JoystickButton(opJoystick, 3)
      .whileTrue(new Climb(1));

    new JoystickButton(opJoystick, 5)
      .whileTrue(new Climb(-1));  


    // box o' joy (ampapult)
    buttonBoard.a()
      .unless(buttonBoard.doorbell())
      .whileTrue(new AmpSetAngle(75.5, false));
    
    buttonBoard.c()
      .whileTrue(new RollerSpin(1.0));

    buttonBoard.f()
      .whileTrue(new RollerSpin(-1.0));
     
    buttonBoard.b()
      .whileTrue(new AmpRotationRaw(0.15));
    
    buttonBoard.e()
      .whileTrue(new AmpRotationRaw(-0.15)); 
      
    buttonBoard.d()
      .unless(buttonBoard.doorbell())
      .whileTrue(new AmpSetAngle(6, false));
  }

  public Command getAutonomousCommand() {
    // get the auto selection from the dropdown on the DS.
    return autoSelect.getSelected();  
  }
}
