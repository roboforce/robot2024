// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.util.function.BooleanSupplier;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.event.EventLoop;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.button.CommandGenericHID;
import edu.wpi.first.wpilibj2.command.button.Trigger;

/** 
 * <p>A class for handling <em>"Rohit and Justin's Box o' Joy"</em>.</p>
 * 
 * <p>In other words, it's a <em>very</em> well made button-board.</p>
 * 
 * <h6>Praise be to Justin Baylis,</h6>
 * <h6>The saviour of our team!</h6>
 */
public class ButtonBoard extends CommandGenericHID {
    private static GenericHID m_hid;

    /**
     * <p>button maps (A, B, C, D ... Doorbell) to what their <em>actual</em> ID is</p>
     * 
     * <p>at a glance:<p>
     * 
     * <ul>
     * <li> [1, 6] - generic buttons </li>
     * <li> [12] is the doorbell </li>
     * </ul>
     */
    public enum BoardButton {
        A(6),
        B(4),
        C(1),
        D(5),
        E(3),
        F(2),
        Doorbell(12);

        public final int id;

        BoardButton(int id) {
            this.id = id;
        }
    }

    /**
     * <p>Trigger, with bonus extensions!</p>
     * 
     * <p>it adds:</p>
     * <ul>
     * <li>unless, a method that changes the trigger to only run if its argument is false.</li>
     * </ul>
     */
    public class BonusTrigger extends Trigger {
        public BonusTrigger(BooleanSupplier condition) {
            super(condition);
        }

        public BonusTrigger(EventLoop loop, BooleanSupplier condition) {
            super(loop, condition);
        }

        /**
         * runs the command only when something isn't true. 
         * @param trigger condition
         * @return new trigger with applied condition
         */
        public BonusTrigger unless(BooleanSupplier trigger) {
            return new BonusTrigger(() -> this.getAsBoolean() && !trigger.getAsBoolean());
        }
    }

    /**
     * makes a new button board. 
     * 
     * @param port port that it is plugged in on
     */
    public ButtonBoard(int port) {
        super(port);

        m_hid = getHID();
    }

    /**
     * Maps a local id (A=0, B=1, C=2, etc...) to the actual button and returns a trigger to handle
     * presses
     * 
     * @param id a number indicating its id on the board
     * @return trigger that is tripped when the button is pressed
     */
    private BonusTrigger portMapped(BoardButton btn) {
        return new BonusTrigger(CommandScheduler.getInstance().getDefaultButtonLoop(), () -> m_hid.getRawButton(btn.id));
    }

    /**
     * the "A" button. 
     * @return a trigger mounted to the A button being pressed. 
     */
    public BonusTrigger a() {
        return this.portMapped(BoardButton.A);
    }

    /**
     * the "B" button. 
     * @return a trigger mounted to the B button being pressed. 
     */
    public BonusTrigger b() {
        return this.portMapped(BoardButton.B);
    }


    /**
     * the "C" button. 
     * @return a trigger mounted to the C button being pressed. 
     */
    public BonusTrigger c() {
        return this.portMapped(BoardButton.C);
    }

    /**
     * the "D" button. 
     * @return a trigger mounted to the D button being pressed. 
     */
    public BonusTrigger d() {
        return this.portMapped(BoardButton.D);
    }


    /**
     * the "E" button. 
     * @return a trigger mounted to the E button being pressed. 
     */
    public BonusTrigger e() {
        return this.portMapped(BoardButton.E);
    }

    /**
     * the "F" button. 
     * @return a trigger mounted to the F button being pressed. 
     */
    public BonusTrigger f() {
        return this.portMapped(BoardButton.F);
    }

    /**
     * the doorbell. 
     * @return a trigger mounted to the doorbell being pressed. 
     */
    public BonusTrigger doorbell() {
        return this.portMapped(BoardButton.Doorbell);
    }
}
