package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/**
 * <p>Climber: hangs on a chain using a mildly adjusted climber-in-a-box. </p>
 * 
 * <p>it has spools and cool hooks</p>
 */
public class Climber extends SubsystemBase {
    //motors
    CANSparkMax l_climber = new CANSparkMax(20, MotorType.kBrushless);
    CANSparkMax r_climber = new CANSparkMax(21, MotorType.kBrushless);

    /** 
     * <p> stored instance.</p>
     * 
     * <p> 
     * this class is a singleton,
     * meaning that there can only ever be one 
     * instance of it at a given time. 
     * </p>
     */
    private static Climber instance;

    /**
     * Makes and/or returns the singleton instance of the {@link Climber}. 
     * @return the {@link Climber} instance. 
     */
    public static Climber GetInstance() {
        // if instance doesn't exist, make a new one.
        if (instance == null) {
            try {
                // safety delay to make sure that the robot is fully up-and-running
                Thread.sleep(50);
            } catch (Exception e) {
                e.printStackTrace();
            }

            instance = new Climber();
        }
        
        // return instance
        return instance;
    }

    /** Creates a new Climber. */
    private Climber() {
        l_climber.restoreFactoryDefaults();
        l_climber.setIdleMode(IdleMode.kBrake);
        
        r_climber.restoreFactoryDefaults();
        r_climber.setIdleMode(IdleMode.kBrake);

        l_climber.follow(r_climber);
    }

    /**
     * sets the speed of the climber motors
     * @param speed the speed, as a percentage (-1.0 to 1.0)
     */
    public void setSpeed(double speed) {
        r_climber.set(speed);
    }

    @Override
    public void periodic() {
        // logs climber motor positions. 
        double rpos = r_climber.getEncoder().getPosition();
        double lpos = l_climber.getEncoder().getPosition();

        SmartDashboard.putNumber("Climber/rightpos", rpos);
        SmartDashboard.putNumber("Climber/leftpos",  lpos);
    }
}
