// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix6.controls.DutyCycleOut;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.signals.NeutralModeValue;

import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/**
 * <p>the main, spinning wheels on the three in one.</p>
 * 
 * <p>this subsystem handles shooting and sucking, thats it. </p>
 * 
 * <p>all rotation is handled by {@link ThreeInOneRot}.</p>
 */
public class ThreeInOneMouth extends SubsystemBase {
  public static final double MOUTH_SPEED = 1.0;

  //duty cycle control. this lets you use some extra ~15% power. 
  private static DutyCycleOut dutyCycleLeft = new DutyCycleOut(
    0, 
    true, 
    false, 
    false, 
    false
  );

  //this came at a cost of $20/motor. god, i love phoenix pro
  private static DutyCycleOut dutyCycleRight = new DutyCycleOut(
    0, 
    true, 
    false, 
    false, 
    false
  ); 

  //motors
  private static TalonFX leftMotor;
  private static TalonFX rightMotor;
  private static CANSparkMax mouthSaw; // teeth

  public DigitalInput noteSwitch = new DigitalInput(0);

  /** 
   * <p> stored instance.</p>
   * 
   * <p> 
   * this class is a singleton,
   * meaning that there can only ever be one 
   * instance of it at a given time. 
   * </p>
   */
  private static ThreeInOneMouth instance; 

  /**
   * Makes and/or returns the singleton instance of the {@link ThreeInOneMouth}. 
   * @return the {@link ThreeInOneMouth} instance. 
   */
  public static ThreeInOneMouth GetInstance() {
    // if instance doesn't exist, make a new one.
    if (instance == null) {
      try {
        // safety delay to make sure that the robot is fully up-and-running
        Thread.sleep(50);
      } catch (Exception e) {
        e.printStackTrace();
      }
      
      instance = new ThreeInOneMouth();
    }

    // return instance
    return instance;
  }

  /** Creates a new ThreeInOneMouth. */
  private ThreeInOneMouth() {
    mouthSaw = new CANSparkMax(22, MotorType.kBrushless);
    mouthSaw.restoreFactoryDefaults();
    mouthSaw.setIdleMode(IdleMode.kCoast);
    mouthSaw.setInverted(false);

    leftMotor = new TalonFX(10);
    leftMotor.setNeutralMode(NeutralModeValue.Coast);
    leftMotor.setInverted(true);

    rightMotor = new TalonFX(11);
    rightMotor.setNeutralMode(NeutralModeValue.Coast);
    rightMotor.setInverted(false);
  }

  /**
   * sets the speed of the motors. (negative is spit, positive is suck)
   * 
   * @param speed speed, in percent from -1.0 to 1.0
   */
  public void setSpeed(double speed) {
    setSpeedBottomOnly(speed);
    setSpeedTopOnly(speed);
  }

  /**
   * sets the speed of the top motors only. 
   * 
   * @param speed speed, in percent from -1.0 to 1.0
   */
  public void setSpeedTopOnly(double speed) {
    mouthSaw.set(speed);
  }

  /**
   * sets the speed of the bottom motors only. 
   * 
   * @param speed speed, in percent from -1.0 to 1.0
   */
  public void setSpeedBottomOnly(double speed) {
    dutyCycleLeft.Output = speed;
    leftMotor.setControl(dutyCycleLeft);

    dutyCycleRight.Output = speed;
    rightMotor.setControl(dutyCycleRight);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putBoolean("note present", !noteSwitch.get());
  }
}
