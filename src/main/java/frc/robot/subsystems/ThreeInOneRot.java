// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix6.configs.Slot0Configs;
import com.ctre.phoenix6.controls.PositionVoltage;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.signals.NeutralModeValue;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/**
 * <p>handles the rotation (pivot) of the three in one.</p>
 * 
 * <p>this is in a seperate subsytem so that it can intake and rotate at the same time.</p>
 * 
 * <p>all shooting logic is contained in {@link ThreeInOneMouth}.</p>
 */
public class ThreeInOneRot extends SubsystemBase {
  //offsets, conversions, and gravity compensation
  private static final double CONVERSION_FACTOR = -1.0 / 5.0 / 9.0 * 18.0 / 42.0 * 360.0;
  private static final double STARTING_ANGLE = 3.5;
  private static double variableOffset = 0.0;
  private static final double GRAVITY_POWER = 0.02;

  //angle setpoints
  public static final double FLOOR_ANGLE = 154.5;
  public static final double SPEAKER_ANGLE = 21.3; // 30.0
  public static final double HOME_ANGLE = 3.5;
  public static final double AMP_ANGLE = 31.3; // 23.5

  //motors (theres only one)
  private static TalonFX pivotMotor;

  //limit switch stuff

  /** starting position limit switch */
  public static DigitalInput start = new DigitalInput(1);

  /** limit switch hard on/off */
  public static final boolean USE_LIMITS = false;

  /** 
   * <p> stored instance.</p>
   * 
   * <p> 
   * this class is a singleton,
   * meaning that there can only ever be one 
   * instance of it at a given time. 
   * </p>
   */
  private static ThreeInOneRot instance;

  /**
   * Makes and/or returns the singleton instance of the {@link ThreeInOneRot}. 
   * @return the {@link ThreeInOneRot} instance. 
   */
  public static ThreeInOneRot GetInstance() {
    // if instance doesn't exist, make a new one.
    if (instance == null) {
      try {
        // safety delay to make sure that the robot is fully up-and-running
        Thread.sleep(50);
      } catch (Exception e) {
        e.printStackTrace();
      } 

      instance = new ThreeInOneRot();
    }

    // return instance
    return instance;
  }

  /** Creates a new ThreeInOne. */
  private ThreeInOneRot() {
    pivotMotor = new TalonFX(9);   
    
    pivotMotor.setNeutralMode(NeutralModeValue.Brake);

    var cfg = pivotMotor.getConfigurator();

    var slot0 = new Slot0Configs();

    slot0.kP = 1.0; 
    slot0.kI = 0.0;
    slot0.kD = 0.0;

    cfg.apply(slot0);
  }

  /**
   * get angle of the motor. 
   * 
   * @return the angle of the motor, in degrees. 
   */
  public double getAngle() {
    return pivotMotor.getPosition().getValueAsDouble() * CONVERSION_FACTOR + STARTING_ANGLE + variableOffset;
  }

  /**
   * 
   * @return gets raw angle w/o variable offset OR starting angle
   */
  public double getAngleRaw() {
    return pivotMotor.getPosition().getValueAsDouble() * CONVERSION_FACTOR;
  }

  /**
   * @return starting angle, w/o variable offset. 
   */
  public static double GetSarting() {
    return STARTING_ANGLE;
  }

  /**
   * get velocity of motor.
   * 
   * @return velocity in RPM.
   */
  public double getVelo() {
    return pivotMotor.getVelocity().getValueAsDouble();
  }

  /**
   * adjusts the angle offset. 
   * @param angle new value for variable offset. 
   */
  public static void setAngleOffset(double angle) {
    variableOffset = angle;
  }

  /**
   * sets the idle/neutral/brakemode of the motor.
   * 
   * @param breakmode true for break, false for coast. 
   */
  public void setBrakeMode(boolean breakmode) {
    if (breakmode)
      pivotMotor.setNeutralMode(NeutralModeValue.Brake);
    else
      pivotMotor.setNeutralMode(NeutralModeValue.Coast);
  } 

  /**
   * speed (negative is up, positive is down).
   * 
   * @param speed percentage from -1.0 to 1.0
   * 
   * @apiNote gravity compensation is enabled by default. 
   */
  public void setSpeed(double speed) {
    setSpeed(speed, true);
  }

  /**
   * speed (negative is up, positive is down).
   * 
   * @param speed percentage from -1.0 to 1.0
   * @param doGravityCompensation if true, adds in gravity compensation.
   */
  public void setSpeed(double speed, boolean doGravityCompensation) {
    if (doGravityCompensation) {
      pivotMotor.set(speed + Math.sin(Math.toRadians(getAngle()))*GRAVITY_POWER);
    } else {
      pivotMotor.set(speed);
    }
  }

  /**
   * set the positione de la motore 
   * 
   * @param degrees angle degrees or else
   */
  public void setPosition(double degrees) { 
    pivotMotor.setControl(
      new PositionVoltage(0)
        .withSlot(0)
        .withPosition((degrees-(STARTING_ANGLE+variableOffset))/CONVERSION_FACTOR)
    );
  }

  // dont repeatedly count limit switch presses 
  private boolean startDown = false;

  @Override
  public void periodic() {
    // This method will be called once per scheduler run

    // put the 3-in-1 arm in coast mode if the roboRIO "user" button is pressed.
    if (RobotController.getUserButton()) {
      setBrakeMode(false);
    }
    
    // log 3-in-1 arm position and voltage use
    double pos = pivotMotor.getPosition().refresh().getValue();
    
    double volts = pivotMotor.getMotorVoltage().getValue();

    SmartDashboard.putNumber("3in1/rotpos-raw", pos);
    SmartDashboard.putNumber("3in1/rotpos-angle", getAngle());
    SmartDashboard.putNumber("3in1/volts", volts);

    // stop using limit switch input based on USE_LIMITS
    if (!start.get() && USE_LIMITS) { 
      if (!startDown)
        setAngleOffset(-getAngleRaw());
    } else {
      startDown = false;
    }
  }
}
