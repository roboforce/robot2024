// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

/**
 * <p>The spinning wheels of the Ampapult, a backup mechanism for scoring amp. </p>
 * 
 * <p>All rotation code can be found in {@link AmpapultRotation}.</p>
 */
public class AmpapultRollers extends SubsystemBase {
  private static CANSparkMax rollerMotor;

  /** 
   * <p> stored instance.</p>
   * 
   * <p> 
   * this class is a singleton,
   * meaning that there can only ever be one 
   * instance of it at a given time. 
   * </p>
   */
  private static AmpapultRollers instance;
  /**
   * Makes and/or returns the singleton instance of the {@link AmpapultRollers}. 
   * @return the {@link AmpapultRollers} instance. 
   */
  public static AmpapultRollers GetInstance() {
    // if instance doesn't exist, make a new one.
    if (instance == null) {
      try {
        // safety delay to make sure that the robot is fully up-and-running
        Thread.sleep(50);
      } catch (Exception e) {
        e.printStackTrace();
      }

      instance = new AmpapultRollers();
    }

    // return instance
    return instance;
  }

  /** Creates a new Ampapult. */
  private AmpapultRollers() {
    rollerMotor = new CANSparkMax(18, MotorType.kBrushless);
    rollerMotor.restoreFactoryDefaults();
    rollerMotor.setIdleMode(IdleMode.kCoast);
  }

  /**
   * sets the speed of the amp rollers. 
   * 
   * @param speed speed of the motor, as a signed percentage of the max speed. 
   */
  public void setSpeedRollers(double speed) {
    rollerMotor.set(speed);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
