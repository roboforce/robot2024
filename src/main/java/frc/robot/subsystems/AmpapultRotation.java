// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.SparkPIDController;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/**
 * <p>Subsystem for handling the Ampapult's rotation.</p>
 * 
 * <p>
 * Similar to the 3-in-1, this is contained in two subsystems to allow 
 * the operator to rotate the Ampapult while intaking/spitting.
 * </p>
 * 
 * <p>All of the code handling the wheels can be found in {@link AmpapultRollers}.</p>
 */
public class AmpapultRotation extends SubsystemBase {
  private static CANSparkMax rotMotor;

  // conversion, positioning, and gravitational constants
  private final static double CONVERSION_FACTOR = -1.0 / 5.0 / 5.0 * 22.0 / 42.0 * 360.0; 
  private final static double STARTING_POSITION = 160;
  private final static double GRAVITY_POWER = 0.025;
  
  /** 
   * <p> stored instance.</p>
   * 
   * <p> 
   * this class is a singleton,
   * meaning that there can only ever be one 
   * instance of it at a given time. 
   * </p>
   */
  private static AmpapultRotation instance;

  /**
   * Makes and/or returns the singleton instance of the {@link AmpapultRotation}. 
   * @return the {@link AmpapultRotation} instance. 
   */
  public static AmpapultRotation GetInstance() {
    // if instance doesn't exist, make a new one.
    if (instance == null) {
      try {
        // safety delay to make sure that the robot is fully up-and-running
        Thread.sleep(50);
      } catch (Exception e) {
        e.printStackTrace();
      }

      instance = new AmpapultRotation();
    }
  
    // return instance
    return instance;
  }

  /** Creates a new Ampapult. */
  private AmpapultRotation() {
    rotMotor = new CANSparkMax(19, MotorType.kBrushless);
    rotMotor.restoreFactoryDefaults();
    rotMotor.setIdleMode(IdleMode.kBrake);

    rotMotor.setOpenLoopRampRate(0.1);

    SparkPIDController pidController = rotMotor.getPIDController();
    pidController.setP(0.2);
    pidController.setD(0.00);
    pidController.setOutputRange(-0.4, 0.4);
  }

  /**
   * set position, in degrees 
   * 
   * @param degrees degrees
   */
  public void setPos(double degrees) {
    rotMotor.getPIDController().setReference((degrees-STARTING_POSITION) / CONVERSION_FACTOR, CANSparkMax.ControlType.kPosition);
  }

  /**
   * set the speed of the motors
   * 
   * @param speed percentage -1.0 to 1.0
   */
  public void setSpeed(double speed){
    setSpeed(speed, true);
  }

  /**
   * sets the speed of the ampapult arm
   * @param speed speed percentage
   * @param doGravityCompensation if false, gravity compensation will not take effect
   */
  public void setSpeed(double speed, boolean doGravityCompensation) {
    if (doGravityCompensation)
      speed += Math.sin(Math.toRadians(getAngle()))*GRAVITY_POWER;
    
    rotMotor.set(speed);
  }

  /**
   * get angle 
   * 
   * @return angle, in degrees
   */
  public double getAngle() {
    return rotMotor.getEncoder().getPosition() * CONVERSION_FACTOR + STARTING_POSITION;
  }

  /**
   * get velocity of motor
   * 
   * @return velocity in RPM
   */
  public double getVelo() {
    return rotMotor.getEncoder().getVelocity();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run

    SmartDashboard.putNumber("ampapult/rotpos-raw", rotMotor.getEncoder().getPosition());
    SmartDashboard.putNumber("ampapult/rotpos-angle", getAngle());
  }
}
