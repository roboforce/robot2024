// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.io.File;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.subsystems.drivetrain.AutoHelper;
import frc.robot.subsystems.drivetrain.SysIdHelper;
import swervelib.SwerveController;
import swervelib.SwerveDrive;
import swervelib.parser.SwerveDriveConfiguration;
import swervelib.parser.SwerveParser;
import swervelib.telemetry.SwerveDriveTelemetry;
import swervelib.telemetry.SwerveDriveTelemetry.TelemetryVerbosity;

/**
 * <p>drivebase class.</p>
 * 
 * <p>this year, we used swerve (usually field centric, unless the gyro got turned off) with YAGSL.</p>
 * 
 * <em>as a warning, this class is really large!</em>
 */
public class Drivetrain extends SubsystemBase {
  // swerve config directory. 
  public static final File SWERVE_DIR = new File(
    Filesystem.getDeployDirectory(), 
    "swerve/"
  );

  // max speed of the robot in m/s. 
  public static final double MAX_SPEED = 3.4;

  // helper classes
  public SysIdHelper sysId;
  public AutoHelper autoHelper;

  // swervedrive object from YAGSL.
  private SwerveDrive swervedrive;

  /**
   * tells the drivetrain to drive in a specified direction, with a specified rotation, in a specified way. 
   * 
   * @param t translation
   * @param rot rotation
   * @param isFieldRel is it field relative
   */
  public void drive(Translation2d t, double rot, boolean isFieldRel) {
    swervedrive.drive(
      t, 
      rot,
      isFieldRel,
      false
    );
  }

  /** 
   * <p> stored instance.</p>
   * 
   * <p> 
   * this class is a singleton,
   * meaning that there can only ever be one 
   * instance of it at a given time. 
   * </p>
   */
  private static Drivetrain instance;

  /**
   * Makes and/or returns the singleton instance of the {@link Drivetrain}. 
   * @return the {@link Drivetrain} instance. 
   */
  public static Drivetrain GetInstance() {
    // if instance doesn't exist, make a new one.
    if (instance == null) {
      try {
        // safety delay to make sure that the robot is fully up-and-running
        Thread.sleep(50);
      } catch (Exception e) {
        e.printStackTrace();
      }

      instance = new Drivetrain();
    }
    
    // return instance
    return instance;
  }

  /** Creates a new Drivetrain. */
  private Drivetrain() {
    SwerveDriveTelemetry.verbosity = TelemetryVerbosity.HIGH;

    try {
      swervedrive = new SwerveParser(SWERVE_DIR).createSwerveDrive(MAX_SPEED);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    swervedrive.setHeadingCorrection(false);
    
    sysId = new SysIdHelper(this);
    autoHelper = new AutoHelper(this);

    autoHelper.configure();
  }

// MARK: getters

  /**
   * @return the SwerveDrive kinematics object.
   */
  public SwerveDriveKinematics getKinematics() {
    return swervedrive.kinematics;
  }

  /**
   * @return the robot's position. 
   */
  public Pose2d getPose() {
    return swervedrive.getPose();
  }

  /**
   * @return robot's yaw (Y-axis rotation) 
   */
  public Rotation2d getYaw() {
    return swervedrive.getYaw();
  }
  
  /**
   * @return robot's yaw, without any offsets. 
   */
  public double getRawYaw() {
    return swervedrive.getGyro().getRawRotation3d().getZ();
  }

  /**
   * get internal swerve interface
   * @return SwerverDrive object. 
   */
  public SwerveDrive getSwerveDrive() {
    return swervedrive;
  }

  /**
   * get the velocity of the robot, field oriented
   * 
   * @return robot velocity in m/s, field relative 
   */
  public ChassisSpeeds getFieldVelo() {
    return swervedrive.getFieldVelocity();
  }

  /**
   * get the velocity of the robot, robot oriented
   * 
   * @return robot velocity in m/s, robot relative
   */
  public ChassisSpeeds getRobotVelo() {
    return swervedrive.getRobotVelocity();
  }

  /**
   * @return the YAGSL {@link SwerveController}. 
   */
  public SwerveController getController() {
    return swervedrive.swerveController;
  }

  /**
   * @return the YAGSL {@link SwerveDriveConfiguration} object. 
   */
  public SwerveDriveConfiguration getCfg() {
    return swervedrive.swerveDriveConfiguration;
  }

  /**
   * makes the robot incredibly difficult to move. 
   */
  public void lock() {
    swervedrive.lockPose();
  }

  /**
   * @return the robots pitch (as in rotation).
   */
  public Rotation2d getPitch() {
    return swervedrive.getPitch();
  }

// MARK: setters

  /** 
   * Sets the desired speed of the drivetrain. 
   * 
   * @param speeds how the drivetrain should move
   */
  public void setChassisSpeeds(ChassisSpeeds speeds) {
    swervedrive.setChassisSpeeds(speeds);
  }

  /**
   * set the gyro angle. this is different from setting the offset 
   * because it takes the current position into account
   * 
   * @param rot the rotation to set the gyroe to 
   */
  public void adjustGyro(Rotation3d rot) {
    swervedrive.setGyro(rot);
  }


  /**
   * set the yaw of the drivetrain, taking into account the current yaw (or maybe not)
   * @param angle angle in radians
   */
  public void adjustGyroYaw(double angle) {
    Rotation3d current = swervedrive.getGyroRotation3d();

    adjustGyro(new Rotation3d(
      current.getX(),
      current.getY(),
      angle
    ));
  }

  /**
   * set the offset of the gyro. 
   * 
   * @param rot the offset to apply to the gyroe
   */
  public void setGyroOffset(Rotation3d rot) {
    swervedrive.setGyroOffset(rot);
  }

  /**
   * set the offset of the gyro, but only affect the yaw. 
   * 
   * @param rot the offset to apply the yaw in radians
   */
  public void setGyroYawOffset(double rot) {
    setGyroOffset(
      new Rotation3d(
        0.0, 
        0.0, 
        rot
      )
    );
  }

  /**
   * set to brake mode the motor
   * 
   * @param brake if its true the robot brake
   */
  public void setIdleMode(boolean brake) {
    swervedrive.setMotorIdleMode(brake);
  }

  /**
   * Puts an indicator on the DS field.
   * 
   * @param trajectory trajectory to put on the DS
   */
  public void postTrajectory(Trajectory trajectory) {
    swervedrive.postTrajectory(trajectory);
  }

// MARK: resetters 

  /**
   * reset the position (odometry) of the robot
   * 
   * @param pose the new position to set it to
   */
  public void resetPose(Pose2d pose) {
    swervedrive.resetOdometry(pose);
  }


  /**
   * reset the position (odometry) of the robot
   */
  public void resetPose() {
    resetPose(
      new Pose2d(
        new Translation2d(0.0, 0.0),
        new Rotation2d(0.0)
      )
    );
  }

  /**
   * reset the gyroscope to zero 
   * 
   * you might need to reset the pose 
   */
  public void resetGyro() {
    swervedrive.zeroGyro();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}