// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.VictorSP;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/**
 * <p>short-lived mechanism to assist in the 3-in-1's ability to score amp.</p>
 */
public class Stick extends SubsystemBase {
  /** speed of the stick. */
  public static final double SPEED = 0.25;

  private VictorSP bagMotor;

  /** 
   * <p> stored instance.</p>
   * 
   * <p> 
   * this class is a singleton,
   * meaning that there can only ever be one 
   * instance of it at a given time. 
   * </p>
   */
  private static Stick instance;

  /**
   * Makes and/or returns the singleton instance of the {@link Stick}. 
   * @return the {@link Stick} instance. 
   */
  public static Stick GetInstance() {
    // if instance doesn't exist, make a new one.
    if (instance == null) {
      try {
        // safety delay to make sure that the robot is fully up-and-running
        Thread.sleep(50);
      } catch (Exception e) {
        e.printStackTrace();
      }
      
      instance = new Stick();
    }

    // return instance
    return instance;
  }

  /** TYLER'S STICK */
  private Stick() {
    bagMotor = new VictorSP(0);
  }

  /**
   * set the speed of the motor
   * @param speed percent max speed can also be not positive
   */
  public void setSpeed(double speed) {
    bagMotor.set(speed);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
