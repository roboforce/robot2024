// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.sysid.SysIdRoutine;
import frc.robot.subsystems.Drivetrain;
import swervelib.SwerveDriveTest;

/** 
 * <p>Performs SysID for Swerve. </p>
 * 
 * <p>a utility class for the {@link Drivetrain}.</p> 
 */
public class SysIdHelper {
    Drivetrain drivetrain; 

    /**
     * Creates a new SysIdHelper. 
     * @param drivetrain drivetrain instance to target. 
     */
    public SysIdHelper(Drivetrain drivetrain) {
        // this has to be done like this since this object is initialized in
        // the constructor for the drivetrain. making it call Drivetrain.GetInstance()
        // would just lead to an infinite loop, and then a crash. 
        //
        // so we just pass in the drivetrain as a parameter. 
        this.drivetrain = drivetrain; 
    }

    /**
     * drives, then spins. 
     * 
     * @return full sysid routine
     */
    public SequentialCommandGroup runSysId() {
        return new SequentialCommandGroup(
            drive(),
            rotation()
        );
    } 

    /**
     * sysid drive routine
     * @return SysID quasistatic, dynamic, back, and forth command
     */
    public Command drive() {
        return SwerveDriveTest.generateSysIdCommand(
            SwerveDriveTest.setDriveSysIdRoutine(
                new SysIdRoutine.Config(), 
                drivetrain,
                drivetrain.getSwerveDrive(),
                12
            ), 
            2.0, 
            5.0, 
            3.0
        );
    }

    /**
     * sysid rotation routine
     * @return SysID quasistatic, dynamic, back, and forth command
     */
    public Command rotation() {
        return SwerveDriveTest.generateSysIdCommand(
            SwerveDriveTest.setAngleSysIdRoutine(
                new SysIdRoutine.Config(), 
                drivetrain, 
                drivetrain.getSwerveDrive()
            ), 
            3.0, 
            5.0,
            5.0
        );
    }
}
