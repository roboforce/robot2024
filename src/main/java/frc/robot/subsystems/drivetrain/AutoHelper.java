// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain;

import java.util.function.DoubleSupplier;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.commands.FollowPathCommand;
import com.pathplanner.lib.util.HolonomicPathFollowerConfig;
import com.pathplanner.lib.util.PIDConstants;
import com.pathplanner.lib.util.ReplanningConfig;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.commands.auto.ScoreSpeaker;
import frc.robot.commands.auto.VariableDelay;
import frc.robot.commands.three_in_one.SpinThreeInOne;
import frc.robot.commands.three_in_one.ThreeInOneToAngle;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.ThreeInOneMouth;
import frc.robot.subsystems.ThreeInOneRot;

/** 
 * Auto-related utility class for the {@link Drivetrain}. 
 */
public class AutoHelper {
    Drivetrain drivetrain;

    /**
     * Creates a new AutoHelper. 
     * @param drivetrain drivetrain instance to target
     */
    public AutoHelper(Drivetrain drivetrain) {
        // this has to be done like this since this object is initialized in
        // the constructor for the drivetrain. making it call Drivetrain.GetInstance()
        // would just lead to an infinite loop, and then a crash. 
        //
        // so we just pass in the drivetrain as a parameter. 
        this.drivetrain = drivetrain;
    }

    /**
     * runs the PathPlanner warmup command. 
     */
    public void warmup() {
        FollowPathCommand.warmupCommand().schedule();
    }

    /**
     * configures autos. 
     */
    public void configure() {
        setupNamedCmds();
        setupHolonomicDrive();
    }

    /**
     * Assigns named commands for pathplanner. 
     */
    private void setupNamedCmds() {
        DoubleSupplier getSelectedSeconds = () -> SmartDashboard.getNumber("auto delay", 0.0);

        //commands that can be used in pathplanner.
        NamedCommands.registerCommand("ScoreSpeaker", new ScoreSpeaker());
        NamedCommands.registerCommand("Suck", new SpinThreeInOne(-ThreeInOneMouth.MOUTH_SPEED));
        NamedCommands.registerCommand("Spit", new SpinThreeInOne(ThreeInOneMouth.MOUTH_SPEED));
        NamedCommands.registerCommand("3in1Floor", new ThreeInOneToAngle(ThreeInOneRot.FLOOR_ANGLE, true));
        NamedCommands.registerCommand("3in1ShootAngle", new ThreeInOneToAngle(ThreeInOneRot.SPEAKER_ANGLE, true));
        NamedCommands.registerCommand("Delay", new VariableDelay(getSelectedSeconds));

        SmartDashboard.putNumber("auto delay", 0.0); // set it so it shows up 
    }

    /**
     * sets up holonomic drive cfg
     */
    private void setupHolonomicDrive() {
        AutoBuilder.configureHolonomic(
            drivetrain::getPose, 
            drivetrain::resetPose, 
            drivetrain::getRobotVelo, 
            (speeds) -> { 
                speeds.omegaRadiansPerSecond /= 1.35;

                drivetrain.setChassisSpeeds(speeds);
            }, 
            new HolonomicPathFollowerConfig(
                new PIDConstants(0.6*5.5, 0.1), 
                new PIDConstants(0.6, 0.0),
                Drivetrain.MAX_SPEED, 
                drivetrain.getCfg().getDriveBaseRadiusMeters(), 
                new ReplanningConfig(),
                0.01
            ), 
            () -> {
                var alliance = DriverStation.getAlliance();
                return alliance.isPresent() ? alliance.get() == DriverStation.Alliance.Red : false;
            }, 
            drivetrain
        );
    }
}
