// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.swerve.OffsetGyro;

/**
 * does nothing, but sets offset position. 
 */
public class DoNothingAuto extends SequentialCommandGroup {
  /** Creates a new DoNothingAuto. */
  public DoNothingAuto(DoubleSupplier angleOffset) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new OffsetGyro(() -> Math.toRadians(angleOffset.getAsDouble()))
    );
  }
}
