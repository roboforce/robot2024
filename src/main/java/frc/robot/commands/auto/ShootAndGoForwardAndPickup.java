// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.swerve.FieldDriveRaw;
import frc.robot.commands.swerve.OffsetGyro;
import frc.robot.commands.swerve.TurnAngle;
import frc.robot.commands.three_in_one.SpinThreeInOne;
import frc.robot.commands.three_in_one.ThreeInOneToAngle;
import frc.robot.subsystems.ThreeInOneMouth;
import frc.robot.subsystems.ThreeInOneRot;

/**
 * untested auto to shoot the preloaded note and pick up a second note. 
 */
public class ShootAndGoForwardAndPickup extends SequentialCommandGroup {
  /** Creates a new ShootAndGoForwardAndPickup. */
  public ShootAndGoForwardAndPickup() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new OffsetGyro(() -> Math.PI), // reset gyro
      new ScoreSpeaker(), // score in the speaker
      new Timed( //move backward so you dont run into speaker
        new FieldDriveRaw(
          new Translation2d(-0.15, 0), 
          0
        ),
        0.5
      ),
      new TurnAngle(0.0), //move to 0 degree
      new ThreeInOneToAngle(ThreeInOneRot.FLOOR_ANGLE, true), // suck preparation
      new ParallelRaceGroup( // suck and go
        new Taxi(() -> Math.PI),
        new SpinThreeInOne(ThreeInOneMouth.MOUTH_SPEED)
      ),
      new ThreeInOneToAngle(ThreeInOneRot.HOME_ANGLE, true), // put three in one back up 
      new TurnAngle(180.0), // turn around 
      new Timed( 
        new FieldDriveRaw(
          new Translation2d(0.18, 0),  //move backward
          0
        ), 
        2.75
      ),
      new ScoreSpeaker() //score again
      //
    );
  }
}
