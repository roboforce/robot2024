// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import java.util.function.DoubleSupplier;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.swerve.FieldDriveRaw;
import frc.robot.commands.swerve.OffsetGyro;

/** 
 * stub for taxying. 
 */
public class Taxi extends SequentialCommandGroup {
  
  /** 
   * Creates a new Taxi. 
   * 
   * @param offset starting angle offset supplier. 
   */
  public Taxi(DoubleSupplier offset) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new OffsetGyro(offset),
      new Timed(
        new FieldDriveRaw(
          new Translation2d(-0.18, 0),
          0
        ), 
        2.25
      )
    );
  }
}
