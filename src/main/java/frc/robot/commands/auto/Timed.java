// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;

/**
 * runs a command for a certain number of seconds. 
 */
public class Timed extends ParallelRaceGroup {
  /** 
   * Runs a command for a certain number of seconds. 
   * 
   * @param command the command to run.
   * @param time how many seconds to run it for.
   */
  public Timed(Command command, double time) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new WaitCommand(time),
      command
    );
  }
}

