// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.Command;

/**
 * lets you manually delay autos by a certain number of seconds. 
 * 
 * operates off of a double supplier, which is checked when the command is first run. 
 */
public class VariableDelay extends Command {
  private Timer timer = new Timer();
  private double seconds = 0.0;
  private DoubleSupplier getSeconds;

  /** 
   * Creates a new VariableDelay. 
   * 
   * @param getSeconds delay supplier. 
   */
  public VariableDelay(DoubleSupplier getSeconds) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.getSeconds = getSeconds;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    seconds = getSeconds.getAsDouble();
    timer.restart();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    timer.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return timer.hasElapsed(seconds);
  }
}
