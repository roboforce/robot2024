// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.swerve.FieldDriveRaw;
import frc.robot.commands.swerve.TurnAngle;
import frc.robot.commands.three_in_one.SpinThreeInOne;
import frc.robot.commands.three_in_one.ThreeInOneToAngle;
import frc.robot.subsystems.ThreeInOneMouth;
import frc.robot.subsystems.ThreeInOneRot;

/**
 * I think that this is the same as {@link PreloadedSpeakerGetNoteFireSpeaker}.
 */
public class SpeakerFirePreloadedGetNoteFire extends SequentialCommandGroup {
  /** Creates a new SpeakerFirePreloadedGetNoteFire. */
  public SpeakerFirePreloadedGetNoteFire() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new ThreeInOneToAngle(ThreeInOneRot.SPEAKER_ANGLE, true),
      new Timed(new SpinThreeInOne(-ThreeInOneMouth.MOUTH_SPEED), 0.75),
      new TurnAngle(0.0), //turn 180 degrees to face note
      new ThreeInOneToAngle(148.5, false), //put intake to ground
      new ParallelRaceGroup(
        new Taxi(() -> Math.PI),
        new SpinThreeInOne(ThreeInOneMouth.MOUTH_SPEED/1.0) //SUCK
      ),
      //new ThreeInOneMouthDoNothing() //stop SUCK
      new ThreeInOneToAngle(ThreeInOneRot.SPEAKER_ANGLE, true),
      new TurnAngle(180.0), //turn back so we face speaker
      new Timed(
        new FieldDriveRaw(
          new Translation2d(0.18, 0),
          0
        ), 
        1.8
      ),
      new ScoreSpeaker() //score note
    );
  }
}
