// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.three_in_one.SpinThreeInOne;
import frc.robot.commands.three_in_one.ThreeInOneToAngle;
import frc.robot.subsystems.ThreeInOneMouth;
import frc.robot.subsystems.ThreeInOneRot;

/**
 * stub for scoring speaker.
 */
public class ScoreSpeaker extends SequentialCommandGroup {
  /** Creates a new ScoreSpeaker. */
  public ScoreSpeaker() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new ThreeInOneToAngle(ThreeInOneRot.SPEAKER_ANGLE, true),
      new WaitCommand(0.45),
      new Timed( 
        new SpinThreeInOne(ThreeInOneMouth.MOUTH_SPEED), 
        1.5
      ),
      new ThreeInOneToAngle(ThreeInOneRot.HOME_ANGLE, true)
    );
  }
}
