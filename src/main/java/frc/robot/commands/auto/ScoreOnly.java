// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.swerve.OffsetGyro;

/**
 * just scores speaker and doesn't taxi. 
 */
public class ScoreOnly extends SequentialCommandGroup {
  /** 
   * Creates a new ScoreOnly.
   * 
   * @param offset starting angle offset supplier. 
   */
  public ScoreOnly(DoubleSupplier offset) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new OffsetGyro(offset),
      new ScoreSpeaker()
    );
  }
}
