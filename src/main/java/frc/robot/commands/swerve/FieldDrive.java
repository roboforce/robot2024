package frc.robot.commands.swerve;

import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.Command;
import swervelib.SwerveController;
import frc.robot.subsystems.Drivetrain;


/**
 * Drives based on joytick input. more information can be found in {@link Drivetrain}.
 */
public class FieldDrive extends Command {
    private final Drivetrain swerve;

    /** gets the x-position of the joystick. */
    private final DoubleSupplier vX;

    /** gets the y-position of the joystick. */
    private final DoubleSupplier vY;

    /** 
     * <p> gets the axis that controlles rotation. </p>
     * 
     * <p> in this case, its the x-value of the right joystick. </p>
     */
    private final DoubleSupplier omega;

    /** 
     * <p> swerve controller obj from {@link Drivetrain}. </p>
     * 
     * <p> used to get the max angular velocity of the robot. </p>
     */
    private final SwerveController controller;
    private final BooleanSupplier fieldRel;
    
    /** 
     * Creates a new FieldDrive.
     * 
     * @param vX x-axis (field relative) supplier
     * @param vY y-axis (field relative) supplier
     * @param omega rotation supplier, usually the x-axis of the second joystick. 
     * @param fieldRel you can run this command either in field relative or robot relative mode.
     */
    public FieldDrive(
        DoubleSupplier vX,
        DoubleSupplier vY,
        DoubleSupplier omega,
        BooleanSupplier fieldRel
        ) {
            this.swerve = Drivetrain.GetInstance();
            this.vX = vX;
            this.vY = vY;
            this.omega = omega;
            this.controller = swerve.getController();
            this.fieldRel = fieldRel;

            //use addRequirements() here to declare subsystem dependencies.
            addRequirements(swerve);
        }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {}

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        // square the inputs for finer control, but also keep track of their sign (+/-).
        double xVelo   = vX.getAsDouble();
        xVelo = Math.pow(xVelo, 2) * Math.signum(xVelo); 
        
        double yVelo   = vY.getAsDouble();
        yVelo = Math.pow(yVelo, 2) * Math.signum(yVelo);
        
        double aVelo   = omega.getAsDouble();
        aVelo = Math.pow(aVelo, 2) * Math.signum(aVelo);

        // drive based on inputs. 
        swerve.drive(
            new Translation2d(
                    xVelo * Drivetrain.MAX_SPEED,
                    yVelo * Drivetrain.MAX_SPEED),
            aVelo * controller.config.maxAngularVelocity * 1.5,
            fieldRel.getAsBoolean()
        );
    }



}
