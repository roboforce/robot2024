// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.swerve;

import java.util.function.DoubleSupplier;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.subsystems.Drivetrain;

/** Offsets the gyroscope for {@link Drivetrain} alignment during autos. */
public class OffsetGyro extends InstantCommand {
  Drivetrain swerve;
  DoubleSupplier offset;

  /** 
   * Creates a new OffsetGyro. 
   * 
   * @param offset an offset that is checked when the command is run, not created. 
   */
  public OffsetGyro(DoubleSupplier offset) {
    swerve = Drivetrain.GetInstance();
    this.offset = offset;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(swerve);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    //if (offset.getAsDouble() == 0.0) return;
    swerve.resetPose(
      new Pose2d(0, 0, new Rotation2d(offset.getAsDouble()))
    );
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
