// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.swerve;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Drivetrain;
import swervelib.SwerveController;

/**
 * <p> drives based on a translation vector and a rotation speed. </p>
 * 
 * <p> these values are expected to be in percentages. </p>
 */
public class FieldDriveRaw extends Command {
  /** translation vector */
  Translation2d movement;

  /** rotation speed */
  double omega;

  /** are we driving field relative? */
  boolean fieldRel;

  // drivetrain stuff
  Drivetrain swerve;
  SwerveController controller;

  /** Creates a new FieldDriveRaw. */
  public FieldDriveRaw(Translation2d movement, double omega) {
    this(movement, omega, true);
  }

  /**
   * Creates a new FieldDriveRaw.
   * 
   * @param movement vector, in percent, of how the robot should move
   * @param omega % of max speed to rotate by
   * @param fieldRel controlls whether or not the robot drives field relative. 
   */
  public FieldDriveRaw(Translation2d movement, double omega, boolean fieldRel) {
    this.movement = movement;
    this.omega = omega;
    this.fieldRel = fieldRel;

    swerve = Drivetrain.GetInstance();
    controller = this.swerve.getController();

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(swerve);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    swerve.drive(
      movement.times(Drivetrain.MAX_SPEED), 
      omega * controller.config.maxAngularVelocity, 
      fieldRel
    );
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
