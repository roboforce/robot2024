// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.swerve;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import frc.robot.subsystems.Drivetrain;

/**
 * "sets" the rotation of the {@link Drivetrain} using a PID loop. 
 */
public class TurnAngle extends PIDCommand {
  /** Creates a new TurnAngle. 
   * @param angle DEGREES!!! DEGREES
  */
  public TurnAngle(double angle) {
    super(
        // The controller that the command will use
        new PIDController(0.06, 0.05, 0.0015),
        // This should return the measurement
        () -> Drivetrain.GetInstance().getPose().getRotation().getDegrees(),
        // This should return the setpoint (can also be a constant)
        () -> angle,
        // This uses the output
        output -> {
          // Use the output here
          Drivetrain.GetInstance()
            .drive(
              new Translation2d(), 
              -output, 
              true
            );
        });

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(Drivetrain.GetInstance());

    // Configure additional PID options by calling `getController` here.

    // sets bound of the loop as [-180°, 180°]. 
    // this lets the values wrap around, like angles normally do. 
    getController().enableContinuousInput(-180, 180); 
    
    // controlls when the command should finish. 
    getController().setTolerance(1, 1);

    // a zone where the i term should come into play. 
    // leaving the i term unrestrained is a *very* bad idea. 
    getController().setIZone(4);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return getController().atSetpoint();
  }
}
