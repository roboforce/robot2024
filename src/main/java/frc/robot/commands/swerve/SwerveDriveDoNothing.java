// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.swerve;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Drivetrain;

public class SwerveDriveDoNothing extends Command {
  private Drivetrain swerve;
  private boolean fieldRel = false;

  /** 
   * <p> do-nothing command for the {@link Drivetrain}. </p> 
   * 
   * <p> 
   * despite what you may have been led to beleive, this is never used.
   * the drivetrain will <em>always</em> either follow an auto path or run {@link FieldDrive}. 
   * </p>
   */
  public SwerveDriveDoNothing(boolean fieldRel) {
    this.fieldRel = fieldRel;
    swerve = Drivetrain.GetInstance();

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(swerve);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    swerve.drive(new Translation2d(), 0, this.fieldRel);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
