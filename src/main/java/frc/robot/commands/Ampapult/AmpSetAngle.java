// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Ampapult;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.AmpapultRotation;

/**
 * <p>"sets" the {@link AmpapultRotation} to a certain point, in radians. </p>
 * 
 * <p> this is handled via a PID. </p>
 */
public class AmpSetAngle extends Command {
  AmpapultRotation rot;
  double pos;
  boolean finishes;  


  /** 
   * Creates a new AmpSetAngle.
   * 
   * @param pos position to "set" the ampapult to, in degrees.
   * @param finishes if true, the command will end. otherwise, it will run indefinitely.
  */
  public AmpSetAngle(double pos, boolean finishes) {
    this.pos = pos;
    this.rot = AmpapultRotation.GetInstance();
    this.finishes = finishes;
  
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(this.rot);
  }


  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    //rot.setPos(pos);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    rot.setPos(pos);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    double velo = Math.abs(rot.getVelo());

    double angle = Math.abs(rot.getAngle()-pos);

    boolean criteria = (angle < 3.5 && velo < 200);

    return (this.finishes && criteria);
  }
}
