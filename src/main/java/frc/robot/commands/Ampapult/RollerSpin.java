// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Ampapult;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.AmpapultRollers;

/**
 * spins the {@link AmpapultRollers}. 
 */
public class RollerSpin extends Command {
  /** Creates a new RollerSpin. */
  AmpapultRollers ampapult;
  double speed;

  /**
   * Creates a new RollerSpin. 
   * 
   * @param speed speed to run the rollers at, as a signed percentage. 
   */
  public RollerSpin(double speed) {
    this.speed = speed;
    ampapult = AmpapultRollers.GetInstance();
    
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ampapult);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    ampapult.setSpeedRollers(speed);

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

    ampapult.setSpeedRollers(0);

  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
