// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.three_in_one;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.auto.Timed;
import frc.robot.subsystems.ThreeInOneMouth;

/**
 * <p>
 * runs top motor of the {@link ThreeInOneMouth} before the bottom ones.
 * </p>
 * 
 * <p>this gives the top motor (the "saw") time to spin up.</p>
 */
public class ShootStaggered extends SequentialCommandGroup {
  /** 
   * Creates a new ShootStaggered. 
   * @param speed speed to run the motors at, as a signed percentage.
   */
  public ShootStaggered(double speed) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new Timed(
        new SpinTop(speed), 
        0.1
      ), 
      new SpinThreeInOne(speed)
    );
  }
}
