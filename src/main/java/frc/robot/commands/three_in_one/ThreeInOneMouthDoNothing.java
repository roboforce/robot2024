// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.three_in_one;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ThreeInOneMouth;

/**
 * default command of the {@link ThreeInOneMouth}. does nothing. 
 */
public class ThreeInOneMouthDoNothing extends Command {
  ThreeInOneMouth mouth;
  /** Creates a new ThreeInOneMouthDoNothing. */
  public ThreeInOneMouthDoNothing() {
    mouth = ThreeInOneMouth.GetInstance();

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(mouth);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    mouth.setSpeed(0.0);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
