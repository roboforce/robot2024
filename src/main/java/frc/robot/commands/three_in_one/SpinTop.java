// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.three_in_one;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ThreeInOneMouth;

/**
 * runs the top motors of the {@link ThreeInOneMouth}.
 */
public class SpinTop extends Command {
  ThreeInOneMouth mouth;
  double speed;
  /** 
   * Creates a new SpinTop. 
   * 
   * @param speed speed to run the top motors at, as a speed percentage.
   */
  public SpinTop(double speed) {
    mouth = ThreeInOneMouth.GetInstance();
    this.speed = speed;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(mouth);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    mouth.setSpeedTopOnly(speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    mouth.setSpeedTopOnly(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
