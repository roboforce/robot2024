// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.three_in_one;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ThreeInOneRot;

/**
 * <p> "sets" the {@link ThreeInOneRot} to a specified angle in degrees. </p>
 * <p> operates based on a PID loop. </p>
 */
public class ThreeInOneToAngle extends Command {
  double pos;
  ThreeInOneRot rot;
  boolean finishes;
  
  /** 
   * Moves three in one to an angle. 
   * 
   * @param pos position to "set" the 3-in-1 to, in degrees.
   * @param finishes if true, the command will end. otherwise, it will run indefinitely.
   */
  public ThreeInOneToAngle(double pos, boolean finishes) {
    this.pos = pos;
    this.finishes = finishes;

    rot = ThreeInOneRot.GetInstance();

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(rot);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    rot.setPosition(pos);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    double velo = Math.abs(rot.getVelo());

    double angle = Math.abs(rot.getAngle()-pos);

    boolean criteria = (angle < 2.0 && velo < 150);

    return (this.finishes && criteria);
  }
}
