// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.three_in_one;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ThreeInOneMouth;
import frc.robot.subsystems.ThreeInOneRot;

/** 
 * <p>spins the {@link ThreeInOneMouth}.</p>
 * 
 * <p>
 * the only reason that it references {@link ThreeInOneRot} is because it used to
 * take the rotation into consideration and set the arm to break mode when intaking
 * (so it hit the floor).</p>
 * 
 * <p>it no longer does this since it didn't work very well.</p>
 */
public class SpinThreeInOne extends Command {
  ThreeInOneMouth mouth;
  ThreeInOneRot rot;
  double speed;

  /** 
   * Creates a new SpinThreeInOne. 
   * 
   * @param speed speed to run the motors at, as a signed percentage. 
   */
  public SpinThreeInOne(double speed) {
    mouth = ThreeInOneMouth.GetInstance();
    rot = ThreeInOneRot.GetInstance();
    this.speed = speed;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(mouth);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    mouth.setSpeed(speed);
   
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() { }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    mouth.setSpeed(0.0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
