// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.util;

import java.util.function.BooleanSupplier;


import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.Subsystem;

/** 
 * Doesn't work and is never used, 
 * but it was intended to be an abstraction of {@link FunctionalCommand}. 
 */
public class SimpleFunctionalCommand extends FunctionalCommand {
  /** 
   * Creates a new SimpleFunctionalCommand. 
   * 
   * @param does function to run
   * @param deps subsystem dependencies
   */
  public SimpleFunctionalCommand(
    Runnable does, 
    Subsystem... deps
  ) {
    super (
      () -> {}, 
      () -> {}, 
      (Boolean j) -> {}, 
      () -> true,
      deps
    );
  }

  /**
   * Creates a new SimpleFunctionalCommand.
   * @param does function to run
   * @param done finishing condition
   * @param deps subsystem dependencies
   */
  public SimpleFunctionalCommand(
    Runnable does,
    BooleanSupplier done,
    Subsystem... deps
  ) {
    super (
      () -> {}, 
      () -> {}, 
      (Boolean j) -> {}, 
      done,
      deps
    );
    // Use addRequirements() here to declare subsystem dependencies.
  }
}
