// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.stick;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Stick;

/**
 * Puts-down/pulls-up the {@link Stick}.
 */
public class Flail extends Command {
  Stick stick; 
  double speed;

  /** 
   * Creates a new Flail. 
   * 
   * @param speed speed of the stick motor, as a signed percentage.
   */
  public Flail(double speed) {
    this.speed = speed;
    stick = Stick.GetInstance();

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(stick);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    stick.setSpeed(speed);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    stick.setSpeed(0.0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
