// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.stick;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.RobotContainer;
import frc.robot.subsystems.Stick;

/**
 * moves the {@link Stick} based on the POV of the operator controller. 
 */
public class FlailWithJoystick extends Command {
  Stick stick;
  
  /** Creates a new FlailWithJoystick. */
  public FlailWithJoystick() {
    stick = Stick.GetInstance();

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(stick);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double pov = RobotContainer.opJoystick.getPOV();
    if(pov > 90 && pov < 270) {
      stick.setSpeed(Stick.SPEED);
    } else if (pov > 270 || (pov > -1 && pov < 90)) {
      stick.setSpeed(-Stick.SPEED);
    } else {
      stick.setSpeed(0.0);
    }

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
