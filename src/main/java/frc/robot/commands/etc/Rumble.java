// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.etc;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.RobotContainer;

/**
 * a command for triggering the joystick to rumble. 
 */
public class Rumble extends Command {
  private double duration;
  private double intensity;
  private RumbleType mode;
  private Timer timer = new Timer();

  /** Rumbles the controller. 
   * @param duration how many seconds to rumble
   * @param intensity how much to rumble, 0.0-1.0;
   * @param mode how to rumble the joystick 
  */
  public Rumble(double duration, double intensity, RumbleType mode) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.duration = duration;
    this.intensity = intensity;
    this.mode = mode;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    timer.restart();
    RobotContainer.driverController.setRumble(mode, intensity);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() { }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    RobotContainer.driverController.setRumble(mode, 0.0);
    timer.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return timer.hasElapsed(duration);
  }
}
