// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.etc;

import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;

/**
 * <p>A warning that will cause the driver controller to vibrate at ~30 seconds.</p>
 * 
 * <p><strong>it does not work.</strong></p>
 */
public class EndgameWarning extends ParallelCommandGroup {
  /** Creates a new EndgameWarning. */
  public EndgameWarning() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands( // 240 BPM - 4/4 
      new WaitUntil(30), 

      new Rumble(0.0625, 0.25, RumbleType.kBothRumble), // 1/16 
      new WaitCommand(0.1875),

      new Rumble(0.125, 0.5, RumbleType.kBothRumble),  // 1/8 
      new WaitCommand(0.125),

      new Rumble(0.1875, 0.75, RumbleType.kBothRumble), // 3/16 
      new WaitCommand(0.0625),

      new Rumble(0.25, 1.0, RumbleType.kBothRumble)    // 1/4
    );
  }
}
