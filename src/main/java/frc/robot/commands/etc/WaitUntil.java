// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.etc;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.Command;

/**
 * A command for waiting until the match timer reached a certain threshold. 
 */
public class WaitUntil extends Command {
  /** time to wait for */
  private int time;

  /** does the command run in teleop? */
  private boolean teleop;

  /** Wait until match timer reaches a certain time. May run in teleop or auto.  
   * 
   * @param time Number of seconds to wait for. In real matches, this is only an integer. 
   * 
   * @param teleop Whether this runs in teleop or auto. 
   * 
   * @apiNote this only works on an actual field. 
  */
  public WaitUntil(int time, boolean teleop) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.time = time;
    this.teleop = teleop;
  }

  /** Wait until match timer reaches a certain time. Only runs in teleop. 
   * 
   * @param time Number of seconds to wait for. In real matches, this is only an integer. 
   * 
   * @apiNote this only works on an actual field. 
  */
  public WaitUntil(int time) {
    this(time, true);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    System.out.println("*********************30 SECONDS LEFT*********************");
    return DriverStation.isAutonomous() == !teleop && DriverStation.getMatchTime() <= time && DriverStation.isFMSAttached();
  }
}
